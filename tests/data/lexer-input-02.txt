dictionary collect:[ : x_90abc:y|x_90abc<y].
x:=(#symbol=#symbol)
"comment" 'string literal'
" should be parsed as: 16rAEFF.012E - 0234e-234 - 16rDEADBEEF + 2e"
x:=16rAEFF.012E-0234e-234-16rDEADBEEF + 2e
x:= a \\ b + #-

"should be parsed as: 45e16 r 23; not: 45e 16r23"
45e16r23


